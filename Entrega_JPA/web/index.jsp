<%-- 
    Document   : login
    Created on : 16/04/2018, 06:37:29 PM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%//eliminar el cache del navegador 
    response.setHeader("Pragma", "no-cache");
    response.addHeader("Cache", "must-revalidate");
    response.addHeader("Cache-control", "no-cache");
    response.addHeader("Cache-control", "no-store");
    response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <title>JAVA WEB JPA</title>
    </head>
    <body>
        <div class="container">
           <div class="row">
                <h3>JAVA WEB JPA - CRUD</h3>
            </div>
            <div class="row">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"></li>                        
                    </ul>                    
                </div>
            </div>
            <div class="row">
                <h3>Iniciar sesion</h3>
            </div>

            <div class="row"> 
                <form class="form-horizontal"  action="Login" method="post">
                    <input type="hidden" name="accion" value="LOGIN" />
                    <div class="control-group">
                        <label class="control-label">USUARIO</label>
                        <div class="controls">
                            <input type="text" name="usuario" id="usuario"  placeholder="Ingresar usuario" value="">                    
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">CLAVE</label>
                        <div class="controls">
                            <input type="password" name="pass" id="pass"  placeholder="Ingresar clave" value="">                    
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Ingresar</button>                        
                    </div>

                </form>
            </div>     
        </div>     
    </body>
</html>
