<%-- 
    Document   : index
    Created on : 14/04/2018, 10:29:01 PM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setHeader("Pragma", "no-cache");
    response.addHeader("Cache", "must-revalidate");
    response.addHeader("Cache-control", "no-cache");
    response.addHeader("Cache-control", "no-store");
    response.setDateHeader("Expires", 0);

    if (session.getAttribute("usuario") == null) {

        try {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } catch (Exception ex) {
            request.getRequestDispatcher("index.jsp").forward(request, response);

        }
    }
%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../css/bootstrap.min.css" type="text/css" rel="stylesheet"/>

        <title>JSP Page</title>
        <script type="text/javascript" src="../../js/reportes.js"></script>
        <script>
            function crearReporte() {
                window.open("../../Reportejasper", "", "width=500,height=500");
            }
        </script>
    </head>
    <body>
        <section class="container">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="" data-toggle="tab">Producto</a></li>
                    <li class=""><a href="" data-toggle="tab">EjemploTab</a></li>
                </ul>
            </div>

            <div>
                <a href="../../Out" class="btn" style="float: right; margin: 5px;">Cerrar Sesión</a>
                <a style="float: right; margin: 5px;"href="insproducto.jsp" class="btn btn-success" >Agregar</a>
                <a style="float: right; margin: 5px;" onclick="createexcel()" class="btn btn-primary" >Exportar Excel</a>
                

            </div>
            <h1>Java web EE</h1>
            <table class="table table-striped table-bordered">
                <thead>
                <th>ID</th>
                <th>CODIGO</th>
                <th>PRODUCTO</th>
                <th>DESCRIPCION</th>
                <th>PRECIO</th>
                <th style="text-align: center;">ACCIONES</th>
                </thead>
                <tbody>
                    <c:forEach var="prf" items="${lista}">
                        <tr>
                            <td>${prf.productId}</td>
                            <td>${prf.code}</td>
                            <td>${prf.name}</td> 
                            <td>${prf.description}</td>
                            <td>${prf.price}</td>
                            <td>

                                <a class="btn"
                                   href="../../Producto?accion=READ&id=${prf.productId}">Detalle</a>
                                <a class="btn btn-danger"
                                   href="../../Producto?accion=GET&id=${prf.productId}">Editar</a>
                                <a class="btn btn-success"
                                   href="../../Producto?accion=DEL&id=${prf.productId}&nombre=${prf.name}">Eliminar</a>


                            </td>
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
        </section>    
    </body>
</html>
