<%-- 
    Document   : delproducto
    Created on : 5/04/2018, 03:52:43 PM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setHeader("Pragma", "no-cache");
    response.addHeader("Cache", "must-revalidate");
    response.addHeader("Cache-control", "no-cache");
    response.addHeader("Cache-control", "no-store");
    response.setDateHeader("Expires", 0);

    if (session.getAttribute("usuario") == null) {

        try {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } catch (Exception ex) {
            request.getRequestDispatcher("index.jsp").forward(request, response);

        }
    }
%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <link   href="css/bootstrap.min.css" rel="stylesheet">
        <link href="../../css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
        <script src="js/bootstrap.min.js"></script>
		    <title>JAVA EE</title>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <h3>JAVA BASICO - CRUD</h3>
            </div>
            <div class="row">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="" data-toggle="tab">Productos</a></li>   
                        
                    </ul>                    
                </div>
            </div>    
            <div class="row">
                <h3>Eliminar producto</h3>
            </div>
            <div class="row">
                <form class="form-horizontal" action="../../Producto" method="post">
                    <input type="hidden" name="accion" value="DELOK"/>
                    <input type="hidden" name="id" value="${id}" />
     
                    <p class="alert alert-error">
                      Esta seguro de eliminar este producto?<br />
                      <b>Nombre</b>: ${nombre}<br /> 
                      
                    </p>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-danger" >Si</button>
                        
                        <a class="btn" href="../../Producto?accion=OK">No</a>
                    </div>
                </form>
            </div>
        </div>

    </body>
</html>