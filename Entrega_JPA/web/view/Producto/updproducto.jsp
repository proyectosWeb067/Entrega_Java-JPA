<%-- 
    Document   : updproducto
    Created on : 6/04/2018, 11:10:38 PM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setHeader("Pragma", "no-cache");
    response.addHeader("Cache", "must-revalidate");
    response.addHeader("Cache-control", "no-cache");
    response.addHeader("Cache-control", "no-store");
    response.setDateHeader("Expires", 0);

    if (session.getAttribute("usuario") == null) {

        try {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } catch (Exception ex) {
            request.getRequestDispatcher("index.jsp").forward(request, response);

        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link   href="../../css/bootstrap.min.css" rel="stylesheet"/>
        
      
		    <title>JAVA EE</title>
    </head>
    <body>
         <body>
        <div class="container">
            <div class="row">
                <h3>JAVA BASICO - CRUD</h3>
            </div>
            <div class="row">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="" data-toggle="tab">Productos</a></li>                        
                    </ul>                    
                </div>
            </div>    
            <div class="row">
                <h3>Actualizar producto</h3>
            </div>
            <div class="row">
                <form class="form-horizontal" action="../../Producto" method="post">
                     <input type="hidden" name="accion" value="UPD"/>
                     <input type="hidden" name="productId" 
                       value="${producto.productId}"/>
                    <div class="control-group">
                        <label class="control-label">CODIGO</label>
                        <div class="controls">
                            <input type="text" name="codigo" id="codigo"  placeholder="Ingresar codigo" value="${producto.code}">                    
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">NOMBRE</label>
                        <div class="controls">
                            <input type="text" name="nombre" id="nombre"  placeholder="Ingresar nombre" value="${producto.name}">                    
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">PRECIO</label>
                        <div class="controls">
                            <input type="text" name="precio" id="precio" placeholder="Ingresar precio" value="${producto.price}">                            
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">DESCRIPCION</label>
                        <div class="controls">
                            <textarea  rows="4" name="description" id="description" placeholder="Ingresar descripcion" >${producto.description}</textarea>                            
                        </div>
                    </div>
                    <div class="form-actions">
                        
                        <a class="btn" href="../../Producto?accion=OK">Cancelar</a>
                      
                        
                        <button type="submit" class="btn btn-success"/>Agregar</button>                      
                    </div>
                </form>
            </div>
        </div>

    </body>

    </body>
</html>