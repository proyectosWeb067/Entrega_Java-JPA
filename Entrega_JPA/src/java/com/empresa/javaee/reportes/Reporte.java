/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.javaee.reportes;
 
 
import com.empresa.proyecto.bean.JavaClassProducto;
import com.empresa.proyecto.entity.Product;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

@WebServlet(name = "Reporte", urlPatterns = {"/Reporteexcel"})
public class Reporte extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/vnd.ms-excel");

        HSSFWorkbook wb = new HSSFWorkbook();       // creando el libro
        HSSFSheet sheet = wb.createSheet("Producto");// creando la hoja
        
        HSSFRow row1 = sheet.createRow(0);	// creaando la fila1
        
        
       sheet.autoSizeColumn((short)0);             //tamaño texto ajustable a celda
        HSSFCell a1 = row1.createCell(0);	 
        HSSFCell b1 = row1.createCell(1);
        HSSFCell c1 = row1.createCell(2);
        HSSFCell d1 = row1.createCell(3);
        HSSFCell e1 = row1.createCell(4);
        
        a1.setCellValue(new HSSFRichTextString("ID"));
        b1.setCellValue(new HSSFRichTextString("CODIGO"));
        c1.setCellValue(new HSSFRichTextString("PRODUCTO"));
        d1.setCellValue(new HSSFRichTextString("DESCRIPCION"));
        e1.setCellValue(new HSSFRichTextString("PRECIO"));
       
        //fuente negrita
         HSSFFont f = wb.createFont();
         f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
         
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle.setFont(f);
        
        a1.setCellStyle(cellStyle);
        b1.setCellStyle(cellStyle);
        c1.setCellStyle(cellStyle);
        d1.setCellStyle(cellStyle);
        e1.setCellStyle(cellStyle);

       JavaClassProducto daop = new JavaClassProducto();
        List<Product> lista = daop.mostrarProducto();
        request.getSession().setAttribute("lista", lista);
        
        int i = 1;
        
        for(Product p: lista){
            
            HSSFRow row = sheet.createRow(i);// creando fila para cuerpo
            
            //para que el texto se adecue a las celdas automaticamente
            sheet.autoSizeColumn((short) 0);
            sheet.autoSizeColumn((short) 1);
            sheet.autoSizeColumn((short) 2);
            sheet.autoSizeColumn((short) 3);
            sheet.autoSizeColumn((short) 4);
            
            HSSFCell cc1 = row.createCell(0);	 
            HSSFCell cc2 = row.createCell(1);
            HSSFCell cc3 = row.createCell(2);
            HSSFCell cc4 = row.createCell(3);
            HSSFCell cc5 = row.createCell(4);

            cc1.setCellValue(p.getProductId()); //estableciendo valores en celda
            cc2.setCellValue(p.getCode());	 
            cc3.setCellValue(p.getName());	 
            cc4.setCellValue(p.getDescription());	 
            cc5.setCellValue(p.getPrice());
            i++;
        }
        
         
        
              
                /* for (int i = 1; i <= lista.size(); i++) {
                    
                     int id = lista.get(0).getProduct_id();
                     String name = lista.get(1).getCode();
                     String code = lista.get(2).getName();
                     String descr = lista.get(3).getDescription();

                     HSSFRow row = sheet.createRow(i);// crea fila2

                     HSSFCell cc1 = row.createCell(0);	// crea A1
                     HSSFCell cc2 = row.createCell(1);
                     HSSFCell cc3 = row.createCell(2);
                     HSSFCell cc4 = row.createCell(3);
                     //HSSFCell cc5 = row.createCell(4);

                     System.out.println("" + name);
                     cc1.setCellValue(id);// A2
                     cc2.setCellValue(code);	// B2
                     cc3.setCellValue(name);	// B2
                     cc4.setCellValue(descr);	// B2
                 }
       
        /*Integer id = lista.get(0).getProduct_id();
        String code = lista.get(1).getCode();
        String name = lista.get(2).getName();
        String descr = lista.get(3).getDescription();
        //Double price = lista.get(4).getPrice();*/

        //for (int i = 1; i <= lista.size(); i++) {

            // cc5.setCellValue(price);	// B2*/
        //}*/

        OutputStream out = response.getOutputStream();
        wb.write(out);
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
