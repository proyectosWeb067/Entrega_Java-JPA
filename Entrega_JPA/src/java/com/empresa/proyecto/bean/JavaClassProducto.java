/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.bean;

import com.empresa.proyecto.controller.ProductJpaController;
import com.empresa.proyecto.controller.exceptions.NonexistentEntityException;
import com.empresa.proyecto.entity.Product;
 
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
 import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * es una mala practica trabajar sobre el controlador(com.empresa.proyeco.controller)
 */
public class JavaClassProducto  {
 
    private EntityManagerFactory emf;
    private ProductJpaController pjc;
    
    public JavaClassProducto(){
        emf = Persistence.createEntityManagerFactory("Entrega_JPAPU");
        pjc = new ProductJpaController(emf);
    }
   
    
    
    //crear un producto
    public void create(Product p){
        pjc.create(p);
    }
  
    //editar producto
    public void edit(Product p){
        try {
            pjc.edit(p);
        } catch (Exception ex) {
            Logger.getLogger(JavaClassProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //eliminar producto por su id
    public void delete(Integer productId){
        try {
            pjc.destroy(productId);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(JavaClassProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //mostrar producto por su id
    public Product getProduct(Integer productId){
        return pjc.findProduct(productId);
    }
    
    //mostrar todos los productos
     public List<Product> mostrarProducto(){
        return pjc.findProductEntities();
    }
}
