/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.servlet;

import com.empresa.proyecto.bean.JavaClassProducto;
import com.empresa.proyecto.entity.Product;
import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 *
 * @ yio
 */
@WebServlet(name = "ServletProducto", urlPatterns = {"/Producto"})
public class ServletProducto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       //parametros 
        String accion = request.getParameter("accion");
        String id = request.getParameter("id");
        request.getSession().setAttribute("id", id);
        String nom = request.getParameter("nombre");
        request.getSession().setAttribute("nombre", nom);
        
        
        
        String tar = null;
        String msg = null;
        JavaClassProducto controlador = new JavaClassProducto();

        if (accion.equals("OK")) {

            List<Product> lista = controlador.mostrarProducto();

            if (lista != null) {
                request.getSession().setAttribute("lista", lista);

                tar = "view/Producto/";

            }
        } else if (accion.equalsIgnoreCase("INS")) {
            Product p = new Product();
            msg = validar(p, request);

            if (msg == null) {
                controlador.create(p);

            }
            if (msg == null) {
                List<Product> lista = controlador.mostrarProducto();
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/";
            }
        } else if (accion.equals("DEL") || accion.equals("DELOK")) {

            tar = "view/Producto/delproducto.jsp";

            if (tar.equals("view/Producto/delproducto.jsp") && accion.equals("DELOK")) {
                controlador.delete(Integer.valueOf(id));
                if (msg == null) {
                    List<Product> lista = controlador.mostrarProducto();
                    request.getSession().setAttribute("lista", lista);

                    tar = "view/Producto/";

                }

            }

        } else if (accion.equals("GET")) {
            String idp = request.getParameter("id");
            Product p = controlador.getProduct(Integer.valueOf(idp));

            if (p != null) {
                request.getSession().setAttribute("producto", p);
                tar = "view/Producto/updproducto.jsp";
            } else {
                msg = "Problemas en obtener datos de Profesor";
            }

        } else if (accion.equals("UPD")) {
            Product p = new Product();
            msg = validar(p, request);

            if (msg == null) {
              controlador.delete(p.getProductId());
            }

            if (msg == null) {
                List<Product> lista = controlador.mostrarProducto();
                request.getSession().setAttribute("lista", lista);
                tar = "view/Producto/index.jsp";
            }
        } else if (accion.equals("READ")) {
            String idp = request.getParameter("id");
            Product p = controlador.getProduct(Integer.valueOf(idp));

            if (p != null) {
                request.getSession().setAttribute("producto", p);
                tar = "view/Producto/readproducto.jsp";
            } else {
                msg = "Problemas en obtener datos de Profesor";
            }

        }

        if (msg != null) {
            request.getSession().setAttribute("msg", msg);
            tar = "mensaje.jsp";
        }
        if (tar != null) {
            response.sendRedirect(tar);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    public String validar(Product p, HttpServletRequest request) {
        String msg;

        String product_id = request.getParameter("productId");
        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String precio = request.getParameter("precio");
        String description = request.getParameter("description");

        msg = ((codigo == null) || (codigo.trim().isEmpty()))
                ? "&iexcl;Ingrese el codigo" : null;

        if (msg == null) {
            msg = ((nombre == null) || (nombre.trim().isEmpty()))
                    ? "&iexcl;Ingrese el nombre" : null;
        }

        if (msg == null) {
            msg = ((precio == null) || (precio.trim().isEmpty()))
                    ? "&iexcl;Ingrese un precio" : null;
        }

        if (msg == null) {

            msg = ((description == null) || (description.trim().isEmpty()))
                    ? "&iexcl;Ingrese una descripción" : null;
        }

        if (msg == null) {
            if (product_id == null) {
                p.setProductId(null);
            } else {
                p.setProductId(Integer.valueOf(product_id));
            }
            p.setCode(codigo);
            p.setName(nombre);
            p.setPrice(Double.parseDouble(precio));
            p.setDescription(description);

        }

        return msg;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
