-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-04-2018 a las 10:21:22
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `productjavaee`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delproducto` (IN `id` INT)  BEGIN
delete from product where product_id = id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getProducto` (IN `id` INT)  BEGIN
select * from product where product_id = id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getProductos` ()  BEGIN
select * from product ORDER BY product_id ASC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insproducto` (IN `cod` VARCHAR(60), IN `nom` VARCHAR(60), IN `descr` VARCHAR(150), IN `pre` DOUBLE(10,2))  BEGIN
insert into product (code,name,description,price) values (cod,nom,descr,pre);
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updproducto` (IN `cod` VARCHAR(60), IN `nom` VARCHAR(60), IN `descr` VARCHAR(100), IN `pri` DOUBLE(10,2), IN `id` INT)  BEGIN
update product set code = cod, name = nom, description = descr,
price = pri where product_id = id;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `PRODUCT_ID` int(10) NOT NULL,
  `CODE` varchar(100) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` text,
  `PRICE` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`PRODUCT_ID`, `CODE`, `NAME`, `DESCRIPTION`, `PRICE`) VALUES
(1, 'PROD01', 'Libro de Spring Security 4', 'El libro contiene una variedad de ejemplos reales..', 25),
(2, 'PROD02', 'JAVA EE ejercicios', 'Libro javaEE', 10.25),
(3, 'PROD03', 'Bases de Datos', 'Libro de bases de datos', 25.5),
(6, 'PROD04', 'Libro Seguridad informatica', 'Libro con ejercicios y ejemplos practicos', 40.3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`PRODUCT_ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `PRODUCT_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
